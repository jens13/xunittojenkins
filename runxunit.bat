@echo off
for /f %%f in ('dir /b /od .\packages\xunit.runners.2.*') do @set xunitfolder=%%f
.\packages\%xunitfolder%\tools\xunit.console.exe %1 -xml .\xunitresult.xml
