﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace xUnitToJenkins.GenerateXml
{
    public class ConsoleRunner
    {
        public static readonly string PathToConsoleRunner;
        public static readonly string[] PathToTestDlls;


        static ConsoleRunner()
        {
            PathToConsoleRunner = Directory.EnumerateFiles(@"..\..\..\", "xunit.console.exe", SearchOption.AllDirectories).FirstOrDefault();
            PathToTestDlls = Directory.EnumerateFiles(@"..\..\..\", "*.Test.dll", SearchOption.AllDirectories).ToArray();
        }

        public static bool ExecuteConsoleRunner()
        {
            string command = string.Join(" ", PathToTestDlls) + @" -xml ..\..\..\xunitresult.xml";
            return Execute(command);
        }

        private static bool Execute(string arguments)
        {
            using (var proc = Process.Start(PathToConsoleRunner, arguments))
            {
                if (proc == null) return false;
                proc.WaitForExit();
                return proc.ExitCode == 0;
            }
        }
    }
}
