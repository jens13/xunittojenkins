﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using xUnitToJenkins.Test.Utility;
using Xunit;

namespace xUnitToJenkins.Test
{
    public class JUnitXmlTest
    {
        [Fact]
        public void JUnitSchemaValitationTest()
        {
            var xml = GenerateJUnitXml();
            Assert.True(ValidateJUnitXml(xml));
        }

        private bool ValidateJUnitXml(XDocument document)
        {
            var schema = XmlReader.Create(new StreamReader(ResourceUtility.LoadResource("junit-7.xsd")));
            return XmlValidator.ValidateXml(document, schema, "");
        }

        private XDocument GenerateJUnitXml()
        {
            var document = XDocument.Load(@"..\..\..\xunitresult.xml");
            var stylesheet = XmlReader.Create(new StreamReader(ResourceUtility.LoadResource("xUnitToJUnit_2b5.xslt")));
            return XmlTransformer.Transform(document, stylesheet);
        }
    }
}