﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace xUnitToJenkins.Test.Utility
{
    public static class XmlValidator
    {
        public static bool ValidateXml(XDocument document, XmlReader schema, string targetNamespace)
        {
            var isValid = true;
            var schemas = new XmlSchemaSet();
            schemas.Add(targetNamespace, schema);
            document.Validate(schemas, (o, e) =>
            {
                Console.WriteLine(e.Message);
                isValid = false;
            });
            Console.WriteLine(isValid ? "Xml is valid" : "Xml is not valid");

            return isValid;
        } 
    }
}