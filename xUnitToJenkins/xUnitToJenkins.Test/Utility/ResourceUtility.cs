﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace xUnitToJenkins.Test.Utility
{
    public static class ResourceUtility
    {
        public static Stream LoadResource(string resourceFilename)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName =
                assembly.GetManifestResourceNames()
                    .FirstOrDefault(n => n.EndsWith(resourceFilename, StringComparison.InvariantCultureIgnoreCase));
            if (string.IsNullOrEmpty(resourceName)) throw new MissingManifestResourceException("Resource not found '" + resourceFilename + "'");

            return assembly.GetManifestResourceStream(resourceName);
        }
    }
}