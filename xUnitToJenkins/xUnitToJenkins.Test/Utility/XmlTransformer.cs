﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace xUnitToJenkins.Test.Utility
{
    public class XmlTransformer
    {
        public static XDocument Transform(XDocument document, XmlReader stylesheet)
        {
            var xslt = new XslCompiledTransform();
            xslt.Load(stylesheet);
            var navigator = document.CreateNavigator();
            using(var stream = new MemoryStream())
            using (XmlWriter result = new XmlTextWriter(stream, Encoding.UTF8))
            {
                xslt.Transform(navigator, result);
                stream.Position = 0;
                return XDocument.Load(stream);
            }
        }
    }
}