﻿using xUnitToJenkins.GenerateXml;
using Xunit;

namespace xUnitToJenkins.Test
{
    public class MiscTest
    {
        [Fact]
        public void ConsoleRunnerTest()
        {
            Assert.NotEmpty(ConsoleRunner.PathToConsoleRunner);
            Assert.True(ConsoleRunner.PathToTestDlls.Length > 1);
        }

        [Fact(Skip = "Skiped test")]
        public void SkipedTest()
        {
            Assert.NotEmpty(ConsoleRunner.PathToConsoleRunner);
            Assert.True(ConsoleRunner.PathToTestDlls.Length > 1);
        }
    }
}