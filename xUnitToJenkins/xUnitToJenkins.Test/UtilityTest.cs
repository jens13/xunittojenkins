﻿using System;
using System.IO;
using System.Resources;
using System.Xml;
using System.Xml.Linq;
using xUnitToJenkins.Test.Utility;
using Xunit;

namespace xUnitToJenkins.Test
{
    public class UtilityTest
    {
        [Fact]
        public void ResourceUtilityTest()
        {
            Assert.NotNull(ResourceUtility.LoadResource("junit-7.xsd"));
        }
        
        [Fact]
        public void ResourceUtilityExceptionTest()
        {
            Assert.Throws<MissingManifestResourceException>(() => ResourceUtility.LoadResource("test"));
        }

        [Fact]
        public void XmlTransformerTest()
        {
            var document = CreateXDocument("<test/>");
            var stylesheet = CreateReader("<test/>");
            XmlTransformer.Transform(document, stylesheet);
        }

        [Fact]
        public void XmlTransformerExceptionTest()
        {
            Assert.Throws<ArgumentNullException>(() => XmlTransformer.Transform(null, null));
            var document = CreateXDocument("<test/>");
            var stylesheet = CreateReader("<test/>");
            Assert.ThrowsAny<Exception>(() => XmlTransformer.Transform(document, stylesheet));
        }

        private static XmlReader CreateReader(string xml)
        {
            return XmlReader.Create(new StringReader(xml));
        }

        private static XDocument CreateXDocument(string xml)
        {
            return XDocument.Load(new StringReader(xml));
        }
    }
}